--RETRIEVING ALL "artists" THAT HAS LETTER "d" ANYWHERE ON ITS NAME
SELECT * FROM artists WHERE name LIKE "%d%";

--RETRIEVING ALL "songs" THAT HAS A LENGTH OF LESS THAN 230
SELECT * FROM songs WHERE length < 230;

--JOINING THE "albums" and "songs" TABLE 
SELECT album_title AS album_name,song_name,length as song_length FROM songs 
JOIN albums ON songs.album_id = albums.id;

--JOINING THE "artists" and "albums" TABLE 
SELECT * FROM artists 
JOIN albums ON artists.id = albums.artist_id and name LIKE "%A%";

--SORTING THE "albums" IN 'Z-A' or DESCENDING ORDER AND LIMIT THE NO. OF RESULTS TO 4
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

--JOINING THE "albums" and "songs" TABLE WITH AN 'ORDER BY' CLAUSE TO ARRANGE THE RESULTS OF "album_title" IN DESCENDING AND "song_name" IN ASCENDING ORDER
SELECT * FROM albums 
JOIN songs ON songs.album_id = albums.id ORDER BY album_title DESC, song_name ASC;

--JOINING THE "artists" and "albums" TABLE AND FILTERING THE RESULTS TO ALBUM TITLE ONLY CONTAINS THE LETTER "A" ANYWHERE IN THE TITLE
SELECT * FROM artists 
JOIN albums ON artists.id = albums.artist_id WHERE albums.album_title LIKE '%A%';